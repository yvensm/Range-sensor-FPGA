
State Machine - |top_range_sensor|range_sensor:range_sens|state_reg
Name state_reg.done_state state_reg.calc_state state_reg.echo_state state_reg.trigger_state state_reg.idle 
state_reg.idle 0 0 0 0 0 
state_reg.trigger_state 0 0 0 1 1 
state_reg.echo_state 0 0 1 0 1 
state_reg.calc_state 0 1 0 0 1 
state_reg.done_state 1 0 0 0 1 

State Machine - |top_range_sensor|bintobcd:bin_to_bcd|state_reg
Name state_reg.done_state state_reg.op state_reg.idle 
state_reg.idle 0 0 0 
state_reg.op 0 1 1 
state_reg.done_state 1 0 1 
