library ieee;
use ieee.std_logic_1164.all;


entity echo_count is

	port(
		clk,reset,start,echo: in std_logic; 
		done: out std_logic;
		dout: out std_logic_vector(12 downto 0)
	);

end echo_count;


architecture arch of echo_count is

component counter_dist is
	generic(
		n: POSITIVE := 10
	);
	port(
		clk: in std_logic;
		enable: in std_logic;
		reset: in std_logic;
		count_out: out std_logic_vector(n-1 downto 0)
	);

end component;

signal notreset: std_logic;
signal done_sig: std_logic :='0';
signal data: std_logic_vector(22 downto 0);

begin
	notreset<=not reset;
	done<=done_sig;
	cnt: counter_dist generic map(23) port map(clk,start,notreset,data);
	process (echo)
		begin
			if(reset = '1') then
				done_sig<='0';
				dout<= (others => '0');
			elsif(echo='1') then
				done_sig<='1';
				dout<=data(12 downto 0);
			end if;
	end process;

end arch;