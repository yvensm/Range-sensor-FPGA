library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity range_sensor is
	port(
		clk,rst,start,pulse_in:in std_logic;
		ready,done,trigg:out std_logic;
		led:out std_logic_vector(3 downto 0);
		data_out:out std_logic_vector(12 downto 0)
	);

end range_sensor;


architecture arch of range_sensor is

  type state_type is (idle, trigger_state,echo_state,calc_state,done_state);
  signal state_reg, state_next : state_type;
  
  --signal t0_reg, t0_next : unsigned (12 downto 0);
  --signal t1_reg, t1_next : unsigned (12 downto 0 );
  --signal n_reg, n_next : unsigned ( 4 downto 0 );  

  
  signal trigger_start: std_logic;
  signal trigger_done: std_logic;
  signal echo_done: std_logic;
  signal echo_en: std_logic;
  signal data: integer;
  
	component trigger_generator is
		port(
			clk: in std_logic;
			trigger: out std_logic;
			trigg_start:in std_logic;
			trigger_done: out std_logic
		);
	end component;
	
	component echo_count is 
		port(
			clk,reset,start,echo: in std_logic;
			done: out std_logic;
			dout: out std_logic_vector(12 downto 0)
		);
	end component;
	
	signal dout: std_logic_vector(12 downto 0);
	signal triggersig: std_logic;
	signal rstecho: std_logic;
begin

	echo_en<= not triggersig;
	rstecho<=triggersig;
	trigg<=triggersig;
 trig: trigger_generator port map(clk,triggersig,trigger_start,trigger_done);
 echo: echo_count port map(clk,rstecho,echo_en,pulse_in,echo_done,dout);
 
-- registradores de estado e de dados
process (clk, rst )
    begin
      if rst = '1' then
        state_reg <= idle;
        --t0_reg <= (others => '0');
        --t1_reg <= (others=>'0');
        --n_reg <= (others=>'0');
    elsif clk'event and clk='1' then
        state_reg <= state_next;
       -- t0_reg <= t0_next;
       --t1_reg <= t1_next;
       -- n_reg  <= n_next;
    end if;
 end process;
 
 
-- lógico do próximo estado
process ( state_reg, start,trigger_done,echo_done)
begin
  ready <='0';
  done <= '0';
  trigger_start<= '0';
  --echo_en<='0';
  state_next <= state_reg;
  led<= "0000";

  case state_reg is
  when idle =>
    ready <= '1';
   if start = '1' then
      state_next <= trigger_state;
		trigger_start<='1';
    end if;
    
  when trigger_state =>
	-- led(0)<='1';
    if trigger_done='1' then
	  --echo_en<='1';
      state_next <= echo_state;
    end if;
    
  when echo_state =>
  	 --led(1)<='1';
	 if echo_done = '1' then
		state_next<= calc_state;
	 end if;
  
  when calc_state =>
 
	 data<= to_integer(unsigned(dout));
--	 data<= data/3;
--	 data<= integer(data);
	
	 state_next<=done_state;

  
  when done_state =>
	led(1)<='1';
	 done<='1';
    state_next <= idle;
	
  end case;
end process;

data_out<=std_logic_vector(to_unsigned(data,13));


end arch;