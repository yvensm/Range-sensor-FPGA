--****************************************************
-- Laborat�rio de Sistemas Digitais
-- Multiplexador de display de sete segmentos
-- Exemplo tirado do livro VHDL by Example (P. Chu)
--
--      IFCE Maracana� - Circuitos Digitais
--****************************************************


library ieee;
use ieee.std_logic_1164.all;
use ieee.numeric_std.all;

entity display_ctr is
  port (
    clk, rst : in std_logic;
    in3, in2, in1, in0: in std_logic_vector( 3 downto 0 );
    an : out std_logic_vector ( 3 downto 0);
    sseg : out std_logic_vector ( 7 downto 0)
  );
end display_ctr;

architecture arch of display_ctr is

--constante para indicar qual bit sera usado para contagem
constant N : integer := 18; 

signal q_reg, q_next : unsigned (N-1 downto 0);
--entrada de selecao do multiplexador
signal sel : std_logic_vector( 1 downto 0);
--saida do multiplexador em bcd
signal mux_out : std_logic_vector ( 3 downto 0);
begin
  --registrador
  process (clk, rst)
    begin
      if rst = '1' then
        q_reg <= (others=>'0');
      elsif clk'event and clk='1' then
        q_reg <= q_next;
      end if;
    end process;
    
    -- logica do proximo estado do contador
    q_next <= q_reg + 1;
    
    -- 2 MSB controlam o multiplexador 4 para 1
    -- e gera o sinal para selecionar o display
    sel <= std_logic_vector(q_reg( N-1 downto N-2));  
    process (sel, in0,in1,in2,in3)
      begin
        case sel is
          when "00" =>
            an <= "0001";
            mux_out<= in0;
          when "01" =>
            an <= "0010";
            mux_out <= in1;
          when "10" =>
            an <= "0100";
            mux_out <= in2;
          when others =>
            an <= "1000";
            mux_out <= in3;
        end case;
      end process;
      
      --conversor de bcd para sete segmentos
      --implementado como tabela verdade usado with..select
      with mux_out select
         sseg( 6 downto 0) <=
             "0111111" when "0000",
             "0000110" when "0001",
             "1011011" when "0010",
             "1001111" when "0011",
				 "1100110" when "0100",
             "1101101" when "0101",
             "1111100" when "0110",
             "0000111" when "0111",
             "1111111" when "1000",
             "1101111" when others;
             
         sseg(7) <= '0';    
    
end arch;