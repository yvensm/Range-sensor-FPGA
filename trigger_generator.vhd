library ieee;
use ieee.std_logic_1164.all;

entity trigger_generator is

	port(
		clk: in std_logic;
		trigger: out std_logic;
		trigg_start:in std_logic;
		trigger_done: out std_logic
	);

end trigger_generator;


architecture behavioral of trigger_generator is


component counter is
	generic(
		n: POSITIVE := 10
	);
	port(
		clk: in std_logic;
		enable: in std_logic;
		reset: in std_logic;
		count_out: out std_logic_vector(n-1 downto 0)
	);
end component;

signal resetCounter: std_logic;
signal outputCounter: std_logic_vector(22 downto 0);
signal trigdone: std_logic:='0';

begin
	trigger_done<=trigdone;
	
	trigg : counter generic map (23) port map (clk,trigg_start,resetCounter,outputCounter);
	process(clk)
	constant ms250: std_logic_vector(21 downto 0) := "1001100010010110100000";--"101111101011110000100000";
	constant ms250and100us: std_logic_vector(21 downto 0) :="1001100010011010011010";--"101111101011111000010100";--"101111101100111110101000";
	
	begin
		if(outputCounter > ms250 and outputCounter < ms250and100us) then
			trigger<= '1';
			trigdone<='0';
		else
			trigger<='0';
			trigdone<='1';
		end if;
		
		if(outputCounter = ms250and100us or outputCounter= "XXXXXXXXXXXXXXXXXXXXXXXX") then
			resetCounter<='0';
		else
			resetCounter<='1';
		end if;
	end process;
	
end behavioral;
	