library ieee;
use ieee.std_logic_1164.all;



entity top_range_sensor is

	port(
		led:out std_logic_vector(3 downto 0);
		clk,rst,echo:in std_logic;
		trigger:out std_logic;
		an : out std_logic_vector ( 3 downto 0);
		sseg : out std_logic_vector ( 7 downto 0)
	);


end top_range_sensor;



architecture arch of top_range_sensor is


component bintobcd is
	port(
		clk : in std_logic;
		rst : in std_logic;
		start : in std_logic;
		bin : in std_logic_vector ( 12 downto 0 );
		ready, done : out std_logic;
		bcd3,bcd2,bcd1,bcd0 : out std_logic_vector (3 downto 0)
	);
end component;

component display_ctr is
	port (
		clk, rst : in std_logic;
		in3, in2, in1, in0: in std_logic_vector( 3 downto 0 );
		an : out std_logic_vector ( 3 downto 0);
		sseg : out std_logic_vector ( 7 downto 0)
	);
end component;

component range_sensor is
	port(
		clk,rst,start,pulse_in:in std_logic;
		ready,done,trigg:out std_logic;
		led:out std_logic_vector(3 downto 0);
		data_out:out std_logic_vector(12 downto 0)
	);

end component;
signal notrst: std_logic;
signal bcd3,bcd2,bcd1,bcd0 :  std_logic_vector (3 downto 0);
signal data: std_logic_vector(12 downto 0);
signal range_done:std_logic;
begin
notrst<= not rst;
bin_to_bcd: bintobcd port map(clk,notrst,range_done,data,open,open,bcd3,bcd2,bcd1,bcd0);
display: display_ctr port map(clk,notrst,bcd3,bcd2,bcd1,bcd0,an,sseg);
range_sens: range_sensor port map(clk,notrst,'1',echo,open,range_done,trigger,led,data);

end arch;